---
title: Cryptocam einrichten
---

<img src="import_key.png" width="300" alt="Import key screen on first open"/>

Nachdem du deinen ersten Schlüssel generiert hast, kannst du Cryptocam öffnen.

Um den Schlüssel zu importieren, folge dem Setup in Cryptocam und scanne den angezeigten
QR Code, oder gehe in `Einstellungen` > `Schlüssel` und klicke den grossen Knopf, um mehr
weitere Schlüssel zu importieren.

Desweiteren muss ein Ordner ausgewählt werden,
in dem Cryptocam die Aufnahmen speichern soll. Du kannst diesen Ordner irgendwo erstellen werden
(SD-Karte, interner Speicher oder auch ein USB-Stick) und dann "Cryptocam Zugriff auf xxx
erlauben" klicken.

<img src="choose_dir1.png" width="300" alt="Choose output folder on first open"/>
<img src="choose_dir2.png" width="300" alt="Pick a location to create the folder"/>
<img src="choose_dir3.png" width="300" alt="Create the output folder"/>
<img src="choose_dir4.png" width="300" alt="Create the output folder"/>

Das waren alle Vorbereitungen, jetzt kannst du Videos aufnehmen!
