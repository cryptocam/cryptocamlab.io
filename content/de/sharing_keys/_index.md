---
title: Schlüssel teilen
---

In Situationen, in denen du oder andere Personen keine Möglichkeit haben,
einen Schlüssel zu generieren, ist es möglich, den öffentlichen Schlüssel
einer **Person, der du vertraust**, zu verwenden (dies ist wichtig,
da sie als einzige in der Lage sein wird, deine Dateien zu entschlüsseln).

Um einen Schlüssel zu teilen muss in den Cryptocam-Einstellungen unter `Schlüssel`
der zu teilende Schlüssel geklickt werden. Dann kann eine andere Person entweder
direkt den angezeigten QR-Code scannen, oder mit dem Knopf neben dem öffentlichen
Schlüssel diesen in die Zwischenablage kopieren.
Der Schlüssel kann dann wie jeder andere Text verschickt werden.

<img src="key_detail.png" width="300">

Um einen Schlüssel von jemensch anderem zu importieren, kann so wie für jeden Schlüssel
der QR-Code gescannt werden.
Um einen Schlüssel in Text-Form zu importieren, klick oben rechts die drei Punkte
und auf "Schlüssel als Text importieren". Dann muss der Schlüssel in das Feld
eingefügt werden und ein Name für den Schlüssel eingegeben werden.
Nachdem "Schlüssel speichern" geklickt wurde, erscheint er in der Liste.

<img src="sharing_keys1.png" width="300">
<img src="sharing_keys2.png" width="300">
