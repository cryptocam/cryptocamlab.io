---
title: Videos auf den PC übertragen
---

Um Dateien von Android-Geräten auf den PC zu übertragen kann
[`android-file-transfer`](https://whoozle.github.io/android-file-transfer-linux/) genutzt
werden. Das Programm ist im Paketmanager der meisten Distributionen erhältlich, aber auch
als AppImage
[verfügbar](https://github.com/whoozle/android-file-transfer-linux/releases).

Jetzt kannst du dein entsperrtes Android-Gerät per USB mit dem PC verbinden und
`android-file-transfer` öffnen. Die USB-Benachrichtigung in Android sollte auf `USB zur
Dateiübertragung` gestellt sein. Es kann auch ein Popup die Bestätigung der
MTP-Verbindung verlangen.

Wähle den Ordner aus, der vorher als Cryptocam-Ausgabeordner ausgewählt wurde. Mit Rechtsklick >
"Download" wird der Inhalt auf den PC geladen.
