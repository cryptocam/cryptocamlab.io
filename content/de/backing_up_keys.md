---
title: Backups von Schlüsseln erstellen
---

Es gibt zwar keinen Grund Cryptocam-Schlüssel über längere Zeiträume zu verwenden, da
sie klein und leicht zu generieren sind, aber Backups können trotzdem nie schaden.

Es kann einfach der gesamte Keyring-Ordner (standardmäßig `/home/user/CryptocamKeyring`
oder `Meine Dockumente\CryptocamKeyring` auf Windows) kopiert und gesichert werden, oder
aber einzelne Dateien aus dem Ordner. Die Dateien haben die gleichen Namen wie die darin
befindlichen Schlüssel.
