---
title: Videos entschlüsseln
---

Im Companion-Programm: die zu entschlüsselnden Dateien können per Drag and drop oder über `Open files`/`Open folder` geöffnet werden. Die Videos erscheinen dann in der Dateien-Liste.

<img src="/companion-screenshots/filesadded.png" width=500 alt="Companion-Programm mit
ausgewählten Dateien">

Um den Ort, an dem die entschlüsselten Videos abgespeichert werden auszuwählen, klick
unten auf `Choose location`.

Jetzt kannst du einfach `Decrypt` klicken. Das Programm nach allen benötigten Passwörtern
fragen.

<img src="/companion-screenshots/decrypting.png" width=500 alt="Videos entschlüsseln">

