---
title: "Cryptocam: Verschlüsselnde Kamera-App für Android"
---

<img src="/video1.png" height="500"/>
<img src="/video2.png" height="500"/>

Cryptocam ermöglicht es, ein Android-Gerät als live-verschlüsselnde Kamera zu
verwenden. Das bedeutet, dass mensch alles aufnehmen kann, ohne zu riskieren, dass
Unbefugte Zugriff auf die Aufnahmen bekommen könnten, falls das Gerät oder die
Speicherkarte verloren geht. Die Videos werden mit Filippo Valsordas [`age`](https://github.com/FiloSottile/age) verschlüsselt (X25519 und ChaCha20).

## Bedrohungsmodell / Threat model

Cryptocam dient dem Schutz vor Angreifer\*innen mit physischem Zugriff auf das
Gerät, nachdem damit Aufnahmen angefertigt wurden (etwa falls das Gerät
gestohlen oder beschlagnahmt wird.) Solch ein\*e Angreifer\*in ist ohne den
geheimen OpenPGP Schlüssel nicht in der Lage, die Videos zu entschlüsseln.

## Was Cryptocam nicht kann

Cryptocam schützt nicht vor dem Verlust von Aufnahmen, falls das Gerät verloren geht oder
die Dateien von einem\*r Angreifer\*in gelöscht werden. Diese\*r wird lediglich nicht in
der Lage sein, die Aufnahmen zu entschlüsseln.

Auch im Falle, dass die entschlüsselten Videos z.B. auf einem Computer gespeichert sind
und ein\*e Angreifer\*in darauf Zugriff erlangt, kann Cryptocam nicht helfen.

__Verschlüsselt zu filmen ist auch kein Grund, Menschen, die dem nicht zustimmen Kameras ins
Gesicht zu halten__. Sie können nicht davon ausgehen, dass eure Aufnahmen verschlüsselt
sind, selbst dann müssen sie darauf vertrauen, dass die Schlüssel sicher verwaltet werden.

## Über diese Seite

Das hier soll eine ausführliche und Anfänger\*innen-freundliche Anleitung zur Verwendung
von Cryptocam sein.

Falls das nicht nötig ist, ist hier die schnelle Version.

{{< expand "Kurzer Schnellstart" >}}
[`cryptocam-companion-cli`](https://gitlab.com/cryptocam/cryptocam-companion-cli)
installieren.

Schlüsselgenerieren:
```
cryptocam key-gen "Name for my key"
```

Der angezeigte QR Code kann dann in Cryptocam gescannt und dadurch der Schlüssel
importiert werden.

Um Cryptocam-Videos zu entschlüsseln müssen alle Dateien aus dem
Cryptocam Output-Ordner auf den PC kopiert werden. Entschlüsselt wird dann mit

```
cryptocam decrypt path/to/files/* -o decrypted/
```

Die entschlüsselten Videos werden in den Ordner `decrypted/` geschrieben.

{{< /expand >}}
