---
title: Schlüssel erzeugen
---

Cryptocam nutzt asymmetrische Verschlüsselung. Das bedeutet, dass zwei Schlüssel
generiert werden: ein öffentlicher Schlüssel (public key) zum verschlüsseln, und ein
geheimer Schlüssel (private key) zum entschlüsseln. Da ein\*e Angreifer\*in in Besitz
des Smartphones nicht in der Lage sein soll, die Aufnahmen zu entschlüsseln, wird alles so
eingerichtet, dass der private key zum entschlüsseln überhaupt nie auf das Gerät kommt.
Zum verschlüsseln braucht Cryptocam ja auch nur den public key.

Um ein Schlüsselpaar zu generieren, öffne das Cryptocam Companion-Programm und klicke
`Manage keys`, dann auf `Create Key`.

<img src="/companion-screenshots/createkey.png" width=300 alt="Fenster zur
Schlüsselerzeugung">

Jetzt muss dem Schlüssel ein Name gegeben werden. Das kann alles sein, er dient nur
verschiedene Schlüssel voneinander zu unterscheiden.

Dann muss **ein starkes Passwort gewählt werden**. Das Passwort wird benutzt, um den
geheimen private key zu verschlüsseln. Ansonsten könnte jede\*r mit Zugriff auf die
Schlüsseldatei eure Daten entschlüsseln. Das Passwort sollte mindestens _20 Zeichen_
oder _4-5 Woerter_ lang sein.

Wenn du jetzt auf `Create key` klickst, sollte der neue Schlüssel in der Liste
sowie ein QR Code erscheinen:

<img src="/companion-screenshots/createdkey.png" width=500 alt="GPG-Fenster mit neuem
Schlüssel">
