---
title: Weitere Optionen
---

## Remove EXIF data from photos

Es ist möglich, die EXIF-Daten von aufgenommenen JPEG-Dateien
zu entfernen.
Darin sind keine Standortdaten enthalten,
sondern hauptsächlich Informationen über die verwendete
Kamera
und alle Metadaten sind ohnehin verschlüsselt und ohne
Schlüssel nicht lesbar.
Trotzdem verhindert diese Option, aus Versehen Dateien mit
identifizierenden Daten zu veröffentlichen.

## Dateinamen anpassen

Cryptocam kann die Namen von produzierten Dateien nach
einem Nutzer\*innendefinierten Muster generieren
um Cryptocam-Dateien wenigstens auf den ersten Blick
zu verschleiern.
Die folgenden Variablen können verwendet werden.
Sie werden durch die jeweiligen Werte ersetzt.

 - `$year`, `$month`, `$day`: aktuelles Datum
 - `$hour`, `$min`, `$sec`: aktuelle Uhrzeit
 - `$uuid`: zufällige UUID
 - `$num`: eine Zahl, die für jede Datei um 1 erhöht wird

Beispiele:

 - `P$year$month$day_$hour$min$secASDF.pdf` ergibt Dateinamen wie `20210926_083421ASDF.pdf`
 - `$num.mp4` ergibt Dateinamen wie `0001.mp4`, `0002.mp4` etc.