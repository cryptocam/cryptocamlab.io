---
title: Schlüssel auf Smartphone übertragen
---

Um Dateien auf und von Android-Geräten zu übertragen kann
[`android-file-transfer`](https://whoozle.github.io/android-file-transfer-linux/) genutzt
werden. Das Programm ist im Paketmanager der meisten Distributionen erhältlich, aber auch
als AppImage
[verfügbar](https://github.com/whoozle/android-file-transfer-linux/releases).

Jetzt kannst du dein entsperrtes Android-Gerät per USB mit dem PC verbinden und
`android-file-transfer` öffnen. Die USB-Benachrichtigung in Android sollte auf `USB zur
Dateiübertragung` gestellt sein. Es kann auch ein Popup die Bestätigung der
MTP-Verbindung verlangen.

Nun kannst du `Upload` klicken und die `.pub` Datei, die im vorherigen Schritt
abgespeichert wurde auswählen.

Wenn du jetzt OpenKeychain öffnest und über `+` -> `Aus Datei importieren` die
`.pub`-Datei auswählst sollte der Schlüssel erscheinen.

Jetzt kannst du Cryptocam öffnen! Beim ersten Start musst du einen Schlüssel aus
OpenKeychain auswählen (den, der gerade importiert wurde), und einen Ordner auswählen,
in dem Cryptocam die Aufnahmen speichern soll. Du kannst diesen Ordner irgendwo erstellen werden
(SD-Karte, interner Speicher oder auch ein USB-Stick) und dann "Cryptocam Zugriff auf xxx
erlauben" klicken.

Das waren alle Vorbereitungen, jetzt kannst du Videos aufnehmen!
