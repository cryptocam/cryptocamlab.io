---
title: Installation
---

## F-Droid installieren

Cryptocam ist auf F-Droid verfügbar, einem Katalog für freie und quelloffene
Android-Apps.

Die F-Droid APK muss von https://f-droid.org/ heruntergeladen werden. Dabei wird eine
Warnung vor gefährlichen Dateitypen erscheinen, die weggeklickt werden muss.

Nach dem Download kann die APK per Klick installiert werden. Nochmals wird ein Popup vor
Apps unbekannter Quelle warnen. Aus dem Popup heraus kannst du die Einstellungen öffnen
und "Unbekannte Quellen erlauben" o.Ä. aktivieren. Wenn du dann zurück gehst solltest du
F-Droid installieren können.

Wenn F-Droid zum ersten Mal geöffnet wird, muss es seine Paketquellen aktualisieren, was
eine Weile dauern kann.

Nachdem F-Droid eingerichtet ist kann [Cryptocam](https://f-droid.org/en/packages/com.tnibler.cryptocam/) installiert werden.

## Companion App installieren

### Linux

Falls du ein graphisches Tool möchtest, um Schlüssel zu erzeugen und verwalten und Videos
zu entschlüsseln, kannst du [Cryptocam Companion](https://gitlab.com/cryptocam/cryptocam-companion) benutzen.

Das Tool ist am einfachsten in Form des AppImage zu
verwenden. Es erfordert keine Installation oder anderen installierten Pakete.

**[Neuestes AppImage herunterladen](https://gitlab.com/cryptocam/cryptocam-companion/-/releases) unter Assets > Other**

Um das AppImage auszuführen, muss es als ausführbar markiert sein. In den meisten
Dateimanagern geht dass durch rechtsklick auf die Datei > Properties/Eigenschaften >
Permissions/Rechte, wo dann ein Haken bei "Ausführbar"/"Executable" gesetzt werden muss.
Alternativ kann `chmod +x pfad/zum/AppImage` in der Kommandozeile ausgeführt werden.

Das AppImage sollte nun per Doppelklick ausführbar sein.

Eine Kommandozeilenversion ist auch verfügbar ([`cryptocam-companion-cli`](https://gitlab.com/cryptocam/cryptocam-companion-cli)).

### Windows

**[Neueste Version herunterladen](https://gitlab.com/cryptocam/cryptocam-companion/-/releases) unter Assets > Other > Windows x86\_64**

Die zip-Datei muss extrahiert werden. Dann kann `CryptocamCompanion.exe` im `bin`-Ordner ausgeführt werden.
Mensch kann auch eine Verknüpfung zur `exe` erstellen, aber sie muss im `bin`-Ordner bleiben.
