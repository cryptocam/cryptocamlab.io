---
title: Decrypting videos
---

Open the Companion App and open the files you want to decrypt by
dropping them into the window, or opening them via `Open files` or `Open folder`.

The videos will appear in the file list.

<img src="/companion-screenshots/filesadded.png" width=500 alt="Companion app with added
files">

Select the location where decrypted videos should be saved by clicking `Choose location`
at the bottom of the window.

Now just click `Decrypt`! You will be asked to enter the passphrases for all keys required
to decrypt the videos.

<img src="/companion-screenshots/decrypting.png" width=500 alt="Decrypting videos">
