---
title: Additional options
---

## EXIF-Daten aus Fotos löschen

There is the option to remove metadata from produced JPEG files.
There is no location saved in them, so they mostly contain
details about the camera and device used.
If you keep EXIF data, it is still encrypted and not readable
without your secret key.
But if you do not want to take the risk of accidentally publishing
files with potentially identifying metadata, you can enable this option.

## Custom output file names

Cryptocam allows generating names for output files following a
user-defined pattern to aid in concealing recordings.
The following variables can be used an will be replaced with
the appropriate values:

 - `$year`, `$month`, `$day`: current date
 - `$hour`, `$min`, `$sec`: current time
 - `$uuid`: random UUID
 - `$num`: number incremented for each file

Example patterns:

 - `P$year$month$day_$hour$min$secASDF.pdf` will produce filenames like `20210926_083421ASDF.pdf`
 - `$num.mp4` will produce filenames like `0001.mp4`, `0002.mp4` etc.