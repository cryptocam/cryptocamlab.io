---
title: Setting up Cryptocam
---

After creating your first key, you're ready to open Cryptocam!

To import the key we just created, open Cryptocam, and follow the setup to scan the qr
code, or go to `Settings` > `Keys` and click the big button to import more keys.

<img src="import_key.png" width="300" alt="Import key screen on first open"/>

Now you need to select the location where Cryptocam will store the recorded files. You
can create a new folder anywhere (SD card, Internal storage or even a USB flash drive)
and click "Allow Cryptocam access to folder".

<img src="choose_dir1.png" width="300" alt="Choose output folder on first open"/>
<img src="choose_dir2.png" width="300" alt="Pick a location to create the folder"/>
<img src="choose_dir3.png" width="300" alt="Create the output folder"/>
<img src="choose_dir4.png" width="300" alt="Grant permission for the folder"/>

And that's it. You're ready to record!
