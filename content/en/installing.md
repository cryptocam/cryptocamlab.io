---
title: Installing
---

## Installing F-Droid

Cryptocam is available from F-Droid, an app repository for free and open source Android
apps.

Download the APK from https://f-droid.org/. You'll likely get a message that downloading
apps from the internet can be dangerous, so dismiss that.

Once downloaded, click on the APK to install it. There will be a popup saying apps from
unknown sources can't be installed and a button to take you to settings. In settings,
toggle the switch saying "Allow unkown sources". When you go back you should be able to
install F-Droid.

The first time you open F-Droid, it needs to sync repositories which can take some time.

Once F-Droid is set up, install [Cryptocam](https://f-droid.org/en/packages/com.tnibler.cryptocam/).

## Installing the Companion App

### Linux

If you want a graphical program to manage keys and decrypt videos, you can use the
[Cryptocam Companion](https://gitlab.com/cryptocam/cryptocam-companion).

The easiest way to use it is to download the
latest AppImage which doesn't require any dependencies or setup.

**[Download latest AppImage here](https://gitlab.com/cryptocam/cryptocam-companion/-/releases) under Assets > Other**

To run the AppImage, it needs to have executable permissions. In most file managers, this
is done by right clicking the file > Properties > Permissions and ticking a checkbox that
says "Executable" or "Can run as program".
You can alternatively run `chmod +x path/to/AppImage` in the terminal.

The AppImage should now run on double click.

For a CLI version of the companion, see [`cryptocam-companion-cli`](https://gitlab.com/cryptocam/cryptocam-companion-cli).

### Windows

**[Download latest build here](https://gitlab.com/cryptocam/cryptocam-companion/-/releases) under Assets > Other > Windows x86\_64**

It's a zip file which you need to extract. Then run `CryptocamCompanion.exe` in the `bin` folder.
You can also create a shortcut for the exe and move that wherever you like. The `exe` needs to stay in the `bin` folder though.
