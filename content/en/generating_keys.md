---
title: Generating keys
---

Cryptocam uses asymmetric encryption, which means we will end up with two keys: a public
key that can encrypt data, and a private or secret key that can decrypt that encrypted
data. Since we want to make it impossible for an attacker to decrypt the videos even if
they have complete access to the phone's storage, we're going to set everything up so the
decryption key is not stored on the device at all. After all, Cryptocam only needs the
public key to encrypt.

To generate a keypair, open the Cryptocam Companion app and click `Manage keys`, and then
`Create key`:

<img src="/companion-screenshots/createkey.png" width=300 alt="Create key window">

You can choose any name to identify the key.

**Now choose a strong passphrase.** This passphrase will be used to encrypt the private key,
otherwise anyone with access to your computer will be able to decrypt your files. At least
*20 characters* or *4-5 words* is a good length.

Now when you click `Create key`, it should appear in the list along with its QR code:

<img src="/companion-screenshots/createdkey.png" width=500 alt="Key window with
new key">

