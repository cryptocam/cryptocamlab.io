---
title: Backing up keys
---

You should probably not use the same key for any extended amount of time, and there's no
reason to since they're small and simple to generate.

Still, making a backup of your keyring is never a bad idea. To do so, you just need
to copy the entire keyring folder (which is `/home/username/CryptocamKeyring` by default or
`My Documents\CryptocamKeyring` on Windows) and copy it to your backup location. If you
want to back up (or transfer) specific keys, you can copy the file with that key's name
inside that folder.
