---
title: 'Cryptocam: Record encrypted videos on Android'
---

<img src="video1.png" height="500" alt="Main recording screen"/>
<img src="video2.png" height="500" alt="Main recording screen"/>

Cryptocam allows you to use an Android device as a camera that encrypts video as it is
recorded.
This means you can record anything without worrying about the footage getting into the
wrong hands in case someone else gets access to your device or memory card.

Encryption is done using [`age`](https://github.com/FiloSottile/age) by Filippo
Valsorda (X25519 and ChaCha20).

## Threat model

Cryptocam is designed to defend against an attacker with physical access to your device
after you've recorded videos. This can be anyone stealing your phone, or authorities
confiscating it. This attacker will not be able to view any of your footage without
knowing the `age` private key that can decrypt the video
files.

### What Cryptocam doesn't do

Cryptocam will not help you get videos back if the device is lost/stolen/confiscated or if
the files are deleted by an attacker. It only makes sure that attacker can't see the
videos.

It will also not help if you store the decrypted videos on your computer at home and an
attacker gains access to that.

__Using Cryptocam to encrypt your footage also doesn't mean you should stick cameras into the
faces of non-consenting people__. They might not know your footage is being encrypted
and even if they did they need to trust you to keep your keys safe and not release
uncensored footage without asking them.

## About this page

This is meant to be a comprehensive guide for beginners on how to use Cryptocam.
If you don't need step by step instructions, here is the short version.

{{< expand  "Super quick guide" >}}
Install [`cryptocam-companion-cli`](https://gitlab.com/cryptocam/cryptocam-companion-cli).

Generate a key:
```
cryptocam key-gen "Name for my key"
```

Scan the qr code that appears in Cryptocam to import the key.

To decrypt files and output them to a new folder called `decrypted`:
```
cryptocam decrypt path/to/files/* -o decrypted/
```
{{< /expand >}}
