---
title: Transferring videos to the PC
---

To transfer files from our Android device, we're going to use
[`android-file-transfer`](https://whoozle.github.io/android-file-transfer-linux/). You can
install it through your distributions package manager or download the latest AppImage
[here](https://github.com/whoozle/android-file-transfer-linux/releases).

Now you can connect your device to the computer via USB, make sure it's unlocked and open
`android-file-transfer`. Make sure the USB notification on Android says `USB for file
transfer` or similar. Android might also prompt you to confirm the MTP connection.

Select the folder you chose as Cryptocam's output directory, right click it and select
"Download". Choose a suitable location to copy the videos to and you're ready to decrypt!
